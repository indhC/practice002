// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Practice002GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PRACTICE002_API APractice002GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
